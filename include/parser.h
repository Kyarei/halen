#pragma once

#include <optional>
#include <vector>

#include "ast.h"
#include "lexer.h"

class Parser {
public:
  Parser(std::istream* fh, bool siwgr) : fh(fh), siwgr(siwgr) {}
  std::optional<std::string> parseId();
  std::optional<Expression> parseExpSimple();
  std::optional<Expression::FunctionCall> parseFcall();
  std::optional<Expression> parseExp();
  std::optional<Statement> parseStmt();
  std::optional<Func> parseFunc();
  std::vector<Func> doParse();

private:
  TokenType next();
  TokenType peek();
  std::istream* fh;
  std::vector<TokenType> remembered;
  size_t position = 0;
  bool siwgr;
};
