#include "parser.h"

#include <iostream>

/*

Function declaration:

cunt <function name> [fuck <argument>]* dick
  <statements>
cock

Statements:

- variable declaration -- automagically initialises
  fuck <var name>
- output character
  damn <var name>
- input character
  twat <var name>
- return a value
  piss <var name>
- store to var
  shit <var name> arse <expression> hell
- store to var (indirect)
  shit arse <var name> arse <expression> hell
- if
  tits <expression> hell <statement>
  tits <expression> hell hell <statement> <statement>
- while
  arse <expression> hell <statement>
- compound statement
  dick <statements> cock
- function call
  cunt <function name> [fuck <argument>]* piss

Expressions:

- variable
  <var name>
- parenthesised expression
  dick <expression> cock
- function call
  cunt <function name> [fuck <argument>]* piss
- arithmetic:
  <expr1> arse <expr2> '+'
  <expr1> shit <expr2> '-'
  <expr1> damn <expr2> '*'
  <expr1> tits <expr2> '/'
  (no operator precedence, fuck you)
- load from memory:
  damn <addr>

*/

std::optional<std::string> Parser::parseId() {
  std::string s;
  while (true) {
    TokenType t = peek();
    if (!isServant(t)) break;
    s += t;
    next();
  }
  if (s.empty()) return std::nullopt;
  return std::move(s);
}

// Assumes that the first `cunt` is already read.
std::optional<Expression::FunctionCall> Parser::parseFcall() {
  auto fname = parseId();
  if (!fname.has_value()) return std::nullopt;
  std::vector<Expression> args;
  while (true) {
    if (peek() != tokens::fuck) break;
    next();
    auto arg = parseExp();
    if (!arg.has_value()) return std::nullopt;
    args.push_back(std::move(*arg));
  }
  if (next() != tokens::piss) return std::nullopt;
  return Expression::FunctionCall{
      std::move(*fname),
      std::move(args),
  };
}

std::optional<Expression> Parser::parseExpSimple() {
  TokenType t = peek();
  switch (t) {
  case tokens::dick: {
    next();
    auto e = parseExp();
    if (!e.has_value()) return std::nullopt;
    if (next() != tokens::cock) return std::nullopt;
    return std::move(e);
  }
  case tokens::cunt: {
    next();
    auto res = parseFcall();
    if (!res.has_value()) return std::nullopt;
    return Expression{std::move(*res)};
  }
  case tokens::damn: {
    next();
    auto e = parseExp();
    if (!e.has_value()) return std::nullopt;
    return Expression{Expression::Load{
        std::make_unique<Expression>(std::move(*e)),
    }};
  }
  default: {
    auto id = parseId();
    if (!id.has_value()) return std::nullopt;
    return Expression{std::move(*id)};
  }
  }
}

std::optional<Expression> Parser::parseExp() {
  auto first = parseExpSimple();
  if (!first.has_value()) return std::nullopt;
  Expression e = std::move(*first);
  while (true) {
    Expression::BinaryOpType type;
    size_t p = position;
    switch (next()) {
    case tokens::arse: type = Expression::BinaryOpType::plus; break;
    case tokens::shit: type = Expression::BinaryOpType::minus; break;
    case tokens::damn: type = Expression::BinaryOpType::times; break;
    case tokens::tits: type = Expression::BinaryOpType::divide; break;
    default: position = p; goto end;
    }
    auto second = parseExpSimple();
    if (!second.has_value()) {
      position = p;
      break;
    }
    e = Expression{Expression::BinaryOp{
        std::make_unique<Expression>(std::move(e)),
        std::make_unique<Expression>(std::move(*second)), type}};
  }
end:
  return std::move(e);
}

std::optional<Statement> Parser::parseStmt() {
  switch (next()) {
  case tokens::fuck: {
    auto v = parseId();
    if (!v.has_value()) return std::nullopt;
    return Statement{Statement::VarDecl{std::move(*v)}};
  }
  case tokens::damn: {
    auto v = parseId();
    if (!v.has_value()) return std::nullopt;
    return Statement{Statement::Output{std::move(*v)}};
  }
  case tokens::twat: {
    auto v = parseId();
    if (!v.has_value()) return std::nullopt;
    return Statement{Statement::Input{std::move(*v)}};
  }
  case tokens::piss: {
    auto v = parseId();
    if (!v.has_value()) return std::nullopt;
    return Statement{Statement::Return{std::move(*v)}};
  }
  case tokens::shit: {
    bool ind = false;
    if (peek() == tokens::arse) {
      next();
      ind = true;
    }
    auto v = parseId();
    if (!v.has_value()) return std::nullopt;
    if (next() != tokens::arse) return std::nullopt;
    auto e = parseExp();
    if (!e.has_value()) return std::nullopt;
    if (next() != tokens::hell) return std::nullopt;
    return Statement{Statement::Store{std::move(*v), std::move(*e), ind}};
  }
  case tokens::tits: {
    auto e = parseExp();
    if (!e.has_value()) return std::nullopt;
    if (next() != tokens::hell) return std::nullopt;
    bool hasElse = false;
    if (peek() == tokens::hell) {
      next();
      hasElse = true;
    }
    auto s1 = parseStmt();
    if (!s1.has_value()) return std::nullopt;
    if (hasElse) {
      auto s2 = parseStmt();
      if (!s2.has_value()) return std::nullopt;
      return Statement{Statement::If{
          std::move(*e),
          std::make_unique<Statement>(std::move(*s1)),
          std::make_unique<Statement>(std::move(*s2)),
      }};
    }
    return Statement{Statement::If{
        std::move(*e),
        std::make_unique<Statement>(std::move(*s1)),
        nullptr,
    }};
  }
  case tokens::arse: {
    auto e = parseExp();
    if (!e.has_value()) return std::nullopt;
    if (next() != tokens::hell) return std::nullopt;
    auto s1 = parseStmt();
    return Statement{Statement::While{
        std::move(*e),
        std::make_unique<Statement>(std::move(*s1)),
    }};
  }
  case tokens::dick: {
    std::vector<Statement> sts;
    while (true) {
      if (peek() == tokens::cock) break;
      auto s = parseStmt();
      if (!s.has_value()) return std::nullopt;
      sts.push_back(std::move(*s));
    }
    next(); // Swallow the cock
    return Statement{Statement::Block{std::move(sts)}};
  }
  case tokens::cunt: {
    auto res = parseFcall();
    if (!res.has_value()) return std::nullopt;
    return Statement{std::move(*res)};
  }
  default: return std::nullopt;
  }
}

std::optional<Func> Parser::parseFunc() {
  if (next() != tokens::cunt) return std::nullopt;
  auto fn = parseId();
  if (!fn.has_value()) return std::nullopt;
  std::vector<std::string> args;
  while (true) {
    if (peek() != tokens::fuck) break;
    next();
    auto arg = parseId();
    if (!arg.has_value()) return std::nullopt;
    args.push_back(std::move(*arg));
  }
  if (next() != tokens::dick) return std::nullopt;
  std::vector<Statement> sts;
  while (true) {
    size_t p = position;
    auto st = parseStmt();
    if (!st.has_value()) {
      position = p;
      break;
    }
    sts.push_back(std::move(*st));
  }
  if (next() != tokens::cock) return std::nullopt;
  return Func{
      std::move(*fn),
      std::move(args),
      std::move(sts),
  };
}

std::vector<Func> Parser::doParse() {
  try {
    std::vector<Func> prog;
    while (true) {
      if (peek() == tokens::endOfFile) break;
      auto f = parseFunc();
      if (!f.has_value()) {
        std::cerr << "FFWC! Ddaeth hi ddim.\n";
        exit(-1);
      }
      prog.push_back(std::move(*f));
      remembered.clear();
      position = 0;
    }
    return prog;
  } catch (const char* st) {
    std::cerr << st << '\n';
    exit(-1);
  }
}

TokenType Parser::next() {
  TokenType t = peek();
  ++position;
  return t;
}

TokenType Parser::peek() {
  if (position < remembered.size()) return remembered[position];
  const char* st;
  TokenType t = siwgr ? lexSiwgr(*fh, st) : lex(*fh, st);
  if (t == tokens::invalid) throw st;
  remembered.push_back(t);
  return t;
}
