#pragma once

#include <stddef.h>
#include <stdint.h>

#include <vector>

#include "ast.h"
#include "validate.h"

// Some random stuff that's abandoned for now.

enum class SSACode {
  phi,
  assign,
  assignInd,
  add,
  sub,
  mul,
  div,
};

struct SSAEntry {
  struct Operand {
    int32_t varnoOrImm;
    bool isVar;
  };
  SSACode code;
  Operand left, right;
  int32_t next;
};

class CFG {
public:
private:
  std::vector<SSAEntry> entries;
};
