#pragma once

#include <stdint.h>

#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "ast.h"

int32_t valueOfName(const std::string& s);

enum class ICode {
  // dest = src1
  assign,
  // mem[src1] = src2
  assignInd,
  // dest = src1 + src2
  add,
  // dest = src1 - src2
  sub,
  // dest = src1 * src2
  mul,
  // dest = src1 / src2
  div,
  // if dest is true, then go to src1, else src2
  jump,
  // output(src1)
  output,
  // dest = src1(args from #src2),
  call,
  // dest = mem[src1]
  load,
  // read char to dest
  input,
};

struct Instruction {
  struct Operand {
    size_t varnoOrImm;
    bool isVar;
  };
  ICode code;
  size_t dest;
  Operand src1, src2;
};

struct VarEntry {
  std::stack<size_t> indices;
};

struct Instructions {
  std::vector<Instruction> out;
  std::unordered_map<std::string, VarEntry> varsByName;
  std::unordered_set<std::string> thisScope;
  std::stack<std::unordered_set<std::string>> pendingScopes;
  std::vector<size_t> pendingReturnLocations;
  size_t nextVarIndex = 1; // 0 is reserved for the return value
  size_t insertVar(const std::string& s) {
    auto [it, succ] = thisScope.insert(s);
    if (!succ) return -1U;
    varsByName[s].indices.push(nextVarIndex++);
    return nextVarIndex - 1;
  }
  size_t lookup(const std::string& s) const {
    auto it = varsByName.find(s);
    if (it == varsByName.end()) return -1U;
    if (it->second.indices.empty()) return -1U;
    return it->second.indices.top();
  }
  void pushScope() {
    pendingScopes.push(std::move(thisScope));
  }
  void popScope() {
    for (const std::string& id : thisScope) {
      varsByName[id].indices.pop();
    }
    thisScope = std::move(pendingScopes.top());
    pendingScopes.pop();
  }
};

struct FunctionContext {
  size_t argCount;
  std::vector<Instruction> def;
};

struct ProgramContext {
  ProgramContext(const std::vector<Func>& functions);
  std::vector<FunctionContext> funcDefs;
  std::unordered_map<std::string, size_t> funcsByName;
};

bool constructInstructions(
    Instructions& out,
    const Func& f, const ProgramContext& pc);

int32_t execute(const ProgramContext& pc);

// namespace bytecode {
//   enum : uint8_t {
//     // STACK:
//     // <- towards bottom | towards top ->
//     // ----------------------------------
//     ret, // [reg]
//     output, // [reg]
//     store, // [reg1] [reg2]
//     storeImm, // [reg1] [imm32]
//     storeInd, // [reg1] [reg2]
//     branch, // [reg] [if] [else]
//     add, // [dest] [src1] [src2]
//     sub,
//     mult,
//     divide,
//   };
// }
