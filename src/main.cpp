#include <string.h>

#include <fstream>
#include <iostream>

#include "lexer.h"
#include "parser.h"
#include "validate.h"

int main(int argc, char** argv) {
  if (argc < 2) {
    std::cerr << "Fuck you, Altera!\n";
    return -1;
  }
  bool siwgr = false;
  if (strcmp(argv[1], "--siwgr") == 0) {
    ++argv;
    siwgr = true;
  }
  std::ifstream fh(argv[1]);
  Parser parser(&fh, siwgr);
  std::vector<Func> prog = parser.doParse();
  ProgramContext pc(prog);
  for (size_t i = 0; i < prog.size(); ++i) {
    Instructions out;
    bool stat = constructInstructions(out, prog[i], pc);
    if (!stat) {
      std::cerr << "Show me your dick!\n";
      return -1;
    }
    pc.funcDefs[i].def = std::move(out.out);
  }
  return execute(pc);
}
