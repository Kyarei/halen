#pragma once

#include <memory>
#include <string>
#include <vector>

// Here, I wish I'd used Rust, because now I have to whip this out again...

#include <variant>

struct Expression {
  struct FunctionCall {
    std::string functionName;
    std::vector<Expression> args;
  };
  enum class BinaryOpType {
    plus, minus, times, divide
  };
  struct BinaryOp {
    std::unique_ptr<Expression> left, right;
    BinaryOpType type;
  };
  struct Load {
    std::unique_ptr<Expression> e;
  };
  std::variant<
    std::string,
    FunctionCall,
    BinaryOp,
    Load
  > data;
};

struct Statement {
  struct VarDecl {
    std::string varName;
  };
  struct Output {
    std::string varName;
  };
  struct Input {
    std::string varName;
  };
  struct Return {
    std::string varName;
  };
  struct Store {
    std::string lhs;
    Expression rhs;
    bool indirect;
  };
  struct If {
    Expression e;
    std::unique_ptr<Statement> t, f;
  };
  struct While {
    Expression e;
    std::unique_ptr<Statement> st;
  };
  struct Block {
    std::vector<Statement> sts;
  };
  std::variant<
    VarDecl,
    Output,
    Input,
    Return,
    Store,
    If,
    While,
    Block,
    Expression::FunctionCall
  > data;
};

struct Func {
  std::string name;
  std::vector<std::string> args;
  std::vector<Statement> body;
};
