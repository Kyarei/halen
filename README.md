## halen

an esoteric programming language named after the Welsh word for 'salt'

## siwgr

a more compact and less vulgar form of halen; use the `--siwgr` flag to enable this
