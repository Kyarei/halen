#include "lexer.h"

#include <string.h>

#include <algorithm>
#include <iostream>
#include <iterator>
#include <string_view>

template<typename It, typename Cmp>
constexpr bool isSorted(It begin, It end, Cmp&& c) {
  if (begin == end) return true;
  It l = begin, r = begin;
  ++r;
  while (r != end) {
    if (!c(*l, *r)) return false;
    ++l;
    ++r;
  }
  return true;
}

/*
  Possible additions for extensions:
  - bastard
  - bitch
  - goddamn (should it be synonymous to 'damn'?)
  - motherfucker (probably for something related to 'fuck')
  - probably not the N-word
  - maybe foreign-language swears
*/

constexpr static std::string_view keywords[] = {
    "arse", // Unlike `ass`, this is actually four letters long
    "cock", "cunt", "damn", "dick", "fuck",
    "hell", "piss", "shit", "tits", "twat",
};

// Names of FGO servants that have spooked kozet, and a few extra they hate.
constexpr static std::string_view servantNames[] = {
    "altera", "altria",    "arthur",    "beowulf",  "blackbeard", "d'eon",
    "emiya",  "heracles",  "karna",     "lancelot", "mh20",       "nitocris",
    "raikou", "siegfried", "spartacus", "tristan",  "vlad3",
};

constexpr size_t nServants = sizeof(servantNames) / sizeof(servantNames[0]);

static_assert(isSorted(
    std::begin(keywords), std::end(keywords), std::less<std::string_view>{}));
static_assert(isSorted(
    std::begin(servantNames), std::end(servantNames),
    std::less<std::string_view>{}));

TokenType appraise(std::string&& s, const char*& err) {
  std::for_each(s.begin(), s.end(), [](char& c) { c = tolower(c); });
  if (s.find("\x6f\x6b\x69\x74\x61") != std::string_view::npos) {
    err = "So you got that Servant. Go play with another fucking program.";
    return tokens::invalid;
  }
  if (s.find("\x66\x75\x6a\x69\x6e\x6f") != std::string_view::npos) {
    err = "I do not have a fucking time machine!";
    return tokens::invalid;
  }
  if (s.find("\x65\x6e\x6b\x69\x64\x75") != std::string_view::npos) {
    err = "That chapter is already closed, fucktard.";
    return tokens::invalid;
  }
  if (s.find("nobu") != std::string_view::npos) {
    err = "Nobu nobu nobu!!!";
    return tokens::invalid;
  }
  while (!s.empty() && ispunct((unsigned char) s.back())) s.pop_back();
  if (s == "\x61\x72\x74\x6f\x72\x69\x61") s = "altria";
  /* local */ {
    auto it = std::lower_bound(std::begin(keywords), std::end(keywords), s);
    if (it != std::end(keywords) && *it == s) return it - std::begin(keywords);
  }
  /* local */ {
    auto it =
        std::lower_bound(std::begin(servantNames), std::end(servantNames), s);
    if (it != std::end(servantNames) && *it == s)
      return it - std::begin(servantNames) + tokens::servants;
  }
  err = "Say that shit to your mum!";
  return tokens::invalid;
}

TokenType lex(std::istream& fh, const char*& err) {
  int c = fh.get();
  // Skip over spaces
  while (isspace(c)) { c = fh.get(); }
  if (c == -1) return tokens::endOfFile;
  std::string s;
  while (c != -1 && !isspace(c)) {
    s += (char) c;
    c = fh.get();
  }
  return appraise(std::move(s), err);
}

TokenType lexSiwgr(std::istream& fh, const char*& err) {
  int c = fh.get();
  // Skip over spaces
  while (isspace(c)) { c = fh.get(); }
  if (c == -1) return tokens::endOfFile;
  if (isalpha(c)) {
    int k = c - 'a';
    if (k >= 0 && k < (int) nServants) return tokens::servants + k;
    return tokens::invalid;
  }
  switch (c) {
  case '!': return tokens::fuck;
  case '@': return tokens::shit;
  case '#': return tokens::dick;
  case '$': return tokens::piss;
  case '%': return tokens::cock;
  case '^': return tokens::cunt;
  case '&': return tokens::tits;
  case '*': return tokens::arse;
  case '(': return tokens::damn;
  case ')': return tokens::hell;
  case '_': return tokens::twat;
  }
  return tokens::invalid;
}
