#pragma once

#include <stdint.h>

#include <iosfwd>
#include <string>

using TokenType = uint8_t;

namespace tokens {
  enum : TokenType {
    arse = 0,
    cock,
    cunt,
    damn,
    dick,
    fuck,
    hell,
    piss,
    shit,
    tits,
    twat,
    nKeywords,
    servants = 128,
    endOfFile = 254,
    invalid = 255,
  };
}

inline bool isServant(TokenType t) {
  return t >= tokens::servants && t < tokens::endOfFile;
}

TokenType appraise(std::string&& s, const char*& err);
TokenType lex(std::istream& fh, const char*& err);
TokenType lexSiwgr(std::istream& fh, const char*& err);
