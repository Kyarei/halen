#include "validate.h"
#include <assert.h>
#include <iostream>

template<typename T>
struct Fail {
  constexpr static bool value = false;
};

ProgramContext::ProgramContext(const std::vector<Func>& functions) {
  for (size_t i = 0; i < functions.size(); ++i) {
    funcsByName.insert({functions[i].name, i});
    funcDefs.push_back({
      functions[i].args.size(),
    });
  }
}

int32_t valueOfName(const std::string& s) {
  int32_t n = 0;
  for (char c : s) {
    int32_t p = (unsigned char) c - 128;
    n = (9 * n) ^ p;
  }
  return n;
}

static size_t constructInstructions(
    Instructions& out,
    const Expression& ex, const ProgramContext& pc);

static size_t constructInstructions(
    Instructions& out,
    const Expression::FunctionCall& arg, const ProgramContext& pc) {
  using Operand = Instruction::Operand;
  auto it = pc.funcsByName.find(arg.functionName);
  if (it == pc.funcsByName.end()) return -1U;
  size_t funcno = it->second;
  const FunctionContext& fc = pc.funcDefs[funcno];
  if (fc.argCount != arg.args.size()) return -1U;
  size_t oldLimit = out.nextVarIndex;
  std::vector<size_t> callArgs;
  callArgs.reserve(fc.argCount);
  for (size_t i = 0; i < fc.argCount; ++i) {
    size_t reg = constructInstructions(out, arg.args[i], pc);
    if (reg == -1U) return -1U;
    callArgs.push_back(reg);
  }
  // Pack args into [oldLimit, oldLimit + fc.argCount)
  for (size_t i = 0; i < fc.argCount; ++i) {
    if (oldLimit + i != callArgs[i])
      out.out.push_back(Instruction{
        ICode::assign, oldLimit + i,
        Operand{callArgs[i], true}, Operand{},
      });
  }
  // Issue the call instruction
  size_t reg = out.nextVarIndex++;
  out.out.push_back(Instruction{
    ICode::call, reg,
    Operand{funcno, false}, Operand{oldLimit, true},
  });
  return reg;
}

static size_t constructInstructions(
    Instructions& out,
    const Expression& ex, const ProgramContext& pc) {
  using Operand = Instruction::Operand;
  return std::visit([&out, &ex, &pc](auto&& arg) -> size_t {
    using T = std::decay_t<decltype(arg)>;
    if constexpr (std::is_same_v<T, std::string>) {
      // Don't need to transfer -- just return the existing register used
      //std::cerr << valueOfName(arg) << " -> " << out.lookup(arg) << '\n';
      return out.lookup(arg);
    } else if constexpr (std::is_same_v<T, Expression::FunctionCall>) {
      return constructInstructions(out, arg, pc);
    } else if constexpr (std::is_same_v<T, Expression::BinaryOp>) {
      size_t l = constructInstructions(out, *(arg.left), pc);
      if (l == -1U) return -1U;
      size_t r = constructInstructions(out, *(arg.right), pc);
      if (r == -1U) return -1U;
      ICode c;
      switch (arg.type) {
        case Expression::BinaryOpType::plus: c = ICode::add; break;
        case Expression::BinaryOpType::minus: c = ICode::sub; break;
        case Expression::BinaryOpType::times: c = ICode::mul; break;
        case Expression::BinaryOpType::divide: c = ICode::div; break;
        default: return -1U;
      }
      size_t reg = out.nextVarIndex++;
      out.out.push_back(Instruction{
        c, reg,
        Operand{l, true}, Operand{r, true},
      });
      return reg;
    } else if constexpr (std::is_same_v<T, Expression::Load>) {
      size_t e = constructInstructions(out, *(arg.e), pc);
      if (e == -1U) return -1U;
      size_t reg = out.nextVarIndex++;
      out.out.push_back(Instruction{
        ICode::load, reg,
        Operand{e, true}, Operand{}
      });
      return reg;
    } else {
      static_assert(Fail<T>::value, "not all options covered");
      return -1U;
    }
  }, ex.data);
}

static bool constructInstructions(
    Instructions& out,
    const std::vector<Statement>& stmts, const ProgramContext& pc);

static bool constructInstructions(
    Instructions& out,
    const Statement& stmt, const ProgramContext& pc) {
  using Operand = Instruction::Operand;
  return std::visit([&out, &stmt, &pc](auto&& arg) -> bool {
    using T = std::decay_t<decltype(arg)>;
    if constexpr (std::is_same_v<T, Statement::VarDecl>) {
      size_t var = out.insertVar(arg.varName);
      if (var == -1U) return false;
      // Now emit an assignment instruction
      out.out.push_back(Instruction{
        ICode::assign, var,
        Operand{(size_t) valueOfName(arg.varName), false},
        Operand{},
      });
      return true;
    } else if constexpr (std::is_same_v<T, Statement::Output>) {
      // Emit an output instruction
      size_t var = out.lookup(arg.varName);
      if (var == -1U) return false;
      out.out.push_back(Instruction{
        ICode::output, 0,
        Operand{var, true},
        Operand{},
      });
      return true;
    } else if constexpr (std::is_same_v<T, Statement::Input>) {
      // Emit an input instruction
      size_t var = out.lookup(arg.varName);
      if (var == -1U) return false;
      out.out.push_back(Instruction{
        ICode::input, var,
        Operand{},
        Operand{},
      });
      return true;
    } else if constexpr (std::is_same_v<T, Statement::Return>) {
      size_t var = out.lookup(arg.varName);
      if (var == -1U) return false;
      out.out.push_back(Instruction{
        ICode::assign, 0,
        Operand{var, true},
        Operand{},
      });
      out.pendingReturnLocations.push_back(out.out.size());
      out.out.push_back(Instruction{
        ICode::jump, 0,
        Operand{}, // Fill this in later
        Operand{},
      });
      return true;
    } else if constexpr (std::is_same_v<T, Statement::Store>) {
      size_t var = out.lookup(arg.lhs);
      if (var == -1U) return false;
      size_t rhs = constructInstructions(out, arg.rhs, pc);
      if (rhs == -1U) return false;
      if (arg.indirect)
        out.out.push_back(Instruction{
          ICode::assignInd, 0,
          Operand{var, true}, Operand{rhs, true},
        });
      else
        out.out.push_back(Instruction{
          ICode::assign, var,
          Operand{rhs, true}, Operand{}
        });
      return true;
    } else if constexpr (std::is_same_v<T, Statement::If>) {
      // (evaluate condition) (jump1) (true block) (jump2) [1] (false block) [2]
      // (evaluate condition) (jump1) (true block) [1]
      size_t reg = constructInstructions(out, arg.e, pc);
      if (reg == -1U) return false;
      size_t j1 = out.out.size();
      out.out.push_back(Instruction{
        ICode::jump, reg,
        Operand{j1 + 1, false}, Operand{-1U, false},
      });
      bool stat = constructInstructions(out, *(arg.t), pc);
      if (!stat) return false;
      size_t j2 = out.out.size();
      // Is there an else-branch?
      if (arg.f != nullptr) {
        out.out.push_back(Instruction{
          ICode::jump, 0,
          Operand{-1U, false}, Operand{-1U, false},
        });
        stat = constructInstructions(out, *(arg.f), pc);
        if (!stat) return false;
        size_t end = out.out.size();
        out.out[j1].src2.varnoOrImm = j2 + 1;
        out.out[j2].src1.varnoOrImm = end;
        out.out[j2].src2.varnoOrImm = end;
      } else {
        out.out[j1].src2.varnoOrImm = j2;
      }
      return true;
    } else if constexpr (std::is_same_v<T, Statement::While>) {
      // [1] (evaluate condition) (jump2) (block) (jump1) [2]
      size_t l1 = out.out.size();
      size_t reg = constructInstructions(out, arg.e, pc);
      if (reg == -1U) return false;
      size_t j2 = out.out.size();
      out.out.push_back(Instruction{
        ICode::jump, reg,
        Operand{out.out.size() + 1, false}, Operand{-1U, false},
      });
      bool stat = constructInstructions(out, *(arg.st), pc);
      if (!stat) return false;
      out.out.push_back(Instruction{
        ICode::jump, 0,
        Operand{l1, false}, Operand{l1, false},
      });
      size_t l2 = out.out.size();
      out.out[j2].src2.varnoOrImm = l2;
      return true;
    } else if constexpr (std::is_same_v<T, Statement::Block>) {
      out.pushScope();
      bool stat = constructInstructions(out, arg.sts, pc);
      out.popScope();
      return stat;
    } else if constexpr (std::is_same_v<T, Expression::FunctionCall>) {
      return constructInstructions(out, arg, pc) != -1U;
    } else {
      // nyi
      static_assert(Fail<T>::value, "not all options covered");
      return false;
    }
  }, stmt.data);
}

static bool constructInstructions(
    Instructions& out,
    const std::vector<Statement>& stmts, const ProgramContext& pc) {
  for (const Statement& stmt : stmts) {
    if (!constructInstructions(out, stmt, pc)) return false;
  }
  return true;
}

bool constructInstructions(
    Instructions& out,
    const Func& f, const ProgramContext& pc) {
  out.pendingScopes.emplace();
  for (const std::string& arg : f.args) {
    if (out.insertVar(arg) == -1U) return false;
  }
  if (!constructInstructions(out, f.body, pc)) return false;
  for (size_t i : out.pendingReturnLocations) {
    Instruction& k = out.out[i];
    k.src1 = k.src2 = Instruction::Operand{out.out.size(), false};
  }
  return true;
}

static int32_t call(const ProgramContext& pc, size_t funcno,
    std::vector<int32_t>& mem, std::vector<int32_t>&& regs) {
  const auto& def = pc.funcDefs[funcno].def;
  size_t i = 0;
  auto getReg = [&regs](size_t no) -> int32_t& {
    if (no >= regs.size()) regs.resize(no + 1);
    return regs[no];
  };
  auto getMem = [&mem](size_t a) -> int32_t& {
    if (a >= mem.size()) mem.resize(a + 1);
    return mem[a];
  };
  auto evalOp = [&getReg](const Instruction::Operand& o) -> int32_t {
    return o.isVar ? getReg(o.varnoOrImm) : (int32_t) o.varnoOrImm;
  };
  while (i < def.size()) {
    const auto& k = def[i];
    switch (k.code) {
      case ICode::add:
        getReg(k.dest) = evalOp(k.src1) + evalOp(k.src2);
        break;
      case ICode::sub:
        getReg(k.dest) = evalOp(k.src1) - evalOp(k.src2);
        break;
      case ICode::mul:
        getReg(k.dest) = evalOp(k.src1) * evalOp(k.src2);
        break;
      case ICode::div:
        getReg(k.dest) = evalOp(k.src1) / evalOp(k.src2);
        break;
      case ICode::load:
        getReg(k.dest) = getMem(evalOp(k.src1));
        break;
      case ICode::assign:
        getReg(k.dest) = evalOp(k.src1);
        break;
      case ICode::assignInd:
        getMem(evalOp(k.src1)) = evalOp(k.src2);
        break;
      case ICode::output:
        std::cout << (char) evalOp(k.src1);
        break;
      case ICode::input:
        getReg(k.dest) = std::cin.get();
        break;
      case ICode::jump:
        i = (getReg(k.dest) > 0 ? evalOp(k.src1) : evalOp(k.src2)) - 1;
        break;
      case ICode::call: {
        std::vector args = { 0 }; // Room for return value
        size_t start = k.src2.varnoOrImm;
        size_t argCount = pc.funcDefs[k.src1.varnoOrImm].argCount;
        for (size_t i = 0; i < argCount; ++i) {
          args.push_back(regs[start + i]);
        }
        getReg(k.dest) = call(pc, k.src1.varnoOrImm, mem, std::move(args));
        break;
      }
      default:
        std::cerr << "Fuck me in the arse!\n";
        exit(-1);
    }
    ++i;
  }
  return getReg(0);
}

int32_t execute(const ProgramContext& pc) {
  auto it = pc.funcsByName.find("\x88"); // This was unintentional.
  if (it == pc.funcsByName.end()) {
    std::cerr << "I cannot find anywhere to stick my throbbing cock!\n";
    exit(-1);
  }
  size_t entry = it->second;
  std::vector<int32_t> mem;
  return call(pc, entry, mem, std::vector<int32_t>());
}
